var zen = {};


zen.createColor = function(r, g, b, opacity) {
	return [r/255, g/255, b/255, opacity/100];
}

zen.degToRad = function(degrees) {
	return (degrees * Math.PI / 180);
}

zen.radToDeg = function(radians) {
	return (radians * 180 / Math.PI);
}