var zenObject = function() {

    this.nVertices = null;
    this.nTriangles = null;
    this.nAttribsPerVertex = null;
    this.triangles = null;
    this.verticesInterleaved = null;

    this.texture = null;

    this.elementBuffer = null;
    this.arrayBuffer = null;
    this.normalBuffer = null;
    this.shaderProgram = null;
    this.position = mat3.create();
    this.modelMatrix = mat4.create();
    this.mvMatrix = mat4.create();
    this.scaleX = null;
    this.scaleY = null;
    this.scaleZ = null;
    this.viewAngle = 0;
    this.direction = null;
    this.vColor = vec4.create();
    this.textureAlpha = 1.0;
};

zenObject.prototype.createFromFile = function(filePath) {
    // deklarace objektu do proměnné, pro vyloučení chyby použití this v jiném scope.
    var mainObject = this;

    // Načítání souboru pomocí jQuery AJAX. 
    // Otestování, zda uživatel zadal adresu a poté se provede načtení JSON objektu
    // Pokud načtení proběhne úspěšně, data se přiřadí do proměnných objektu,
    // ze kterého je metoda volaná
    if (filePath.length) {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            async: false,
            url: filePath,
            success: function(data) {
                mainObject.nVertices = data.nVertices;
                mainObject.nTriangles = data.nTriangles;
                mainObject.nAttribsPerVertex = data.nAttribsPerVertex;
                mainObject.triangles = new Uint16Array(data.triangles);

                /*for (var i = data.verticesInterleaved.length - 1; i >= 0; i--) {
                    data.verticesInterleaved[i] = data.verticesInterleaved[i];
                }*/

                mainObject.verticesInterleaved = new Float32Array(data.verticesInterleaved);



                mainObject.createElementBuffer();
                mainObject.createArrayBuffer();
                mat4.identity(mainObject.modelMatrix);
                mat4.identity(mainObject.mvMatrix);

                console.log("Data z adresy: " + filePath + " byly úspěšně načteny.");

            },
            error: function(xhr, textStatus, error) {
                alert("Při načítání došlo k chybě. Prosím zkontrolujte URL adresu.");
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            }
        });
    } else {
        console.log("Nepodařilo se načíst data z adresy: " + filePath);
    }


};

zenObject.prototype.attachTextureFromFile = function(filePath) {
    this.texture = pgr.createTexture(filePath);
};

zenObject.prototype.createElementBuffer = function() {
    this.elementBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.elementBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, this.triangles, gl.STATIC_DRAW);
};

zenObject.prototype.createArrayBuffer = function() {
    this.arrayBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, this.arrayBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, this.verticesInterleaved, gl.STATIC_DRAW);
};

zenObject.prototype.updateMV = function() {
    mat4.multiply(this.mvMatrix, viewMatrix, this.modelMatrix);
};

zenObject.prototype.draw = function() {
    gl.useProgram(glProgram);
    gl.bindBuffer(gl.ARRAY_BUFFER, this.arrayBuffer);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.elementBuffer);

    gl.bindTexture(gl.TEXTURE_2D, this.texture);


    gl.enableVertexAttribArray(glProgram.texCoordLocation);
    gl.vertexAttribPointer(
        glProgram.texCoordLocation, 2, gl.FLOAT, false, 8 * this.verticesInterleaved.BYTES_PER_ELEMENT, 6 * this.verticesInterleaved.BYTES_PER_ELEMENT
    );

    gl.enableVertexAttribArray(glProgram.aVertexNormal);
    gl.vertexAttribPointer(glProgram.aVertexNormal, 3, gl.FLOAT, false, 8 * this.verticesInterleaved.BYTES_PER_ELEMENT, 3 * this.verticesInterleaved.BYTES_PER_ELEMENT);

    //this.useMVMatrix();

    if (this.textureAlpha != 1) {
        gl.enable(gl.BLEND);
        gl.disable(gl.DEPTH_TEST);
    } else {
        gl.disable(gl.BLEND);
        gl.enable(gl.DEPTH_TEST);
    }

    gl.enableVertexAttribArray(glProgram.positionLocation);
    mat4.multiply(this.mvMatrix, viewMatrix, this.modelMatrix);
    gl.uniformMatrix4fv(glProgram.mvMatrixUniform, false, this.mvMatrix);
    gl.uniform1f(glProgram.uTextureAlphaValue, this.textureAlpha);

    gl.vertexAttribPointer(glProgram.positionLocation, 3, gl.FLOAT, gl.FALSE, 8 * this.verticesInterleaved.BYTES_PER_ELEMENT, 0);
    gl.drawElements(gl.TRIANGLES, this.nTriangles * 3, gl.UNSIGNED_SHORT, 0);


};

zenObject.prototype.translate = function(x, y, z) {
    this.position = [x, y, z];
    mat4.translate(this.modelMatrix, this.modelMatrix, this.position);
};

zenObject.prototype.moveForward = function(amount) {
    this.position = [x, y, z];
    mat4.translate(this.mvMatrix, this.mvMatrix, this.position);
};

zenObject.prototype.rotateX = function(degrees) {
    if (degrees >= -360 && degrees <= 360) {
        this.viewAngle += degrees;
        //this.direction = vec3(Math.cos(viewAngle)), Math.sin(this.viewAngle), 0.0);
        mat4.rotateX(this.modelMatrix, this.modelMatrix, zen.degToRad(degrees));
    }
};

zenObject.prototype.rotateY = function(degrees) {
    if (degrees >= -360 && degrees <= 360) {
        mat4.rotateY(this.modelMatrix, this.modelMatrix, zen.degToRad(degrees));
    }
};

zenObject.prototype.rotateZ = function(degrees) {
    if (degrees >= -360 && degrees <= 360) {
        mat4.rotateZ(this.modelMatrix, this.modelMatrix, zen.degToRad(degrees));
    }
};

zenObject.prototype.scale = function(x, y, z) {
    if (x && y && z) {
        this.scaleX = x;
        this.scaleY = y;
        this.scaleZ = z;
        mat4.scale(this.modelMatrix, this.modelMatrix, [this.scaleX, this.scaleY, this.scaleZ]);
    } else if (x && !y && !z) {
        this.scaleX = x;
        this.scaleY = x;
        this.scaleZ = x;
        mat4.scale(this.modelMatrix, this.modelMatrix, [this.scaleX, this.scaleY, this.scaleZ]);
    } else {
        return 1;
    }
}
