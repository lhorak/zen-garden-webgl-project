var zenCamera = function() {

    this.upVector = vec3.create(0,1,0);
    this.cameraMatrix = mat4.create();
    this.cameraCenter = vec3.create();
    this.cameraPosition = vec3.create();
    this.azimuth    = 0.0;
    this.elevation  = 0.0;
    this.direction = vec3.create();
    this.position = vec3.create();


    this.basicSetup = function(position) {
    	this.cameraMatrix = mat4.identity(viewMatrix);

    	this.cameraPosition = [0,0,0];

    	this.upVector = [0,1,0];

    	

        viewMatrix = this.cameraMatrix;

        this.direction = [Math.cos(this.azimuth), 0, Math.sin(this.azimuth)];

        this.cameraCenter = this.direction;

        this.position = position;

        this.update();


    }

    this.setPosition = function(p){
        vec3.set(p, this.position);
	}

	this.setAzimuth = function(az){
	    this.changeAzimuth(az - this.azimuth);
	}

	this.changeAzimuth = function(az){
	    var c = this;
	    c.azimuth +=az;
	    
	    if (c.azimuth > 360 || c.azimuth < -360) {
			c.azimuth = c.azimuth % 360;
		}

        this.cameraCenter[0] = Math.cos(zen.degToRad(this.azimuth));
        this.cameraCenter[2] = Math.sin(zen.degToRad(this.azimuth));
	}

	this.setElevation = function(el){
	    this.changeElevation(el - this.elevation);
	}

	this.changeElevation = function(el){
	    var c = this;
	    
	    c.elevation +=el;
	    
	    if (c.elevation > 360 || c.elevation <-360) {
			c.elevation = c.elevation % 360;
		}
	    c.update();
	}

    this.turnUp = function(angle) {
    	this.changeElevation(angle);
    	this.update();
    } 

    this.turnDown = function(angle) {
    	this.changeElevation(-angle);
    	this.update();
    } 

    this.turnLeft = function(angle) {
    	
    	this.changeAzimuth(-angle);

        //console.log("Rotace kamery: " + this.azimuth + "°");
        

        //console.log("Vektor pohledu: [" + this.cameraCenter[0] + "," + this.cameraCenter[1] + "," + this.cameraCenter[2] );

        this.update();

        //mat4.rotateY(viewMatrix, viewMatrix, zen.degToRad(90));
    } 

    this.turnRight = function(angle) {


    	this.changeAzimuth(angle);

        //console.log("Rotace kamery: " + this.azimuth + "°");

        
        

        //console.log("Vektor pohledu: [" + this.cameraCenter[0] + "," + this.cameraCenter[1] + "," + this.cameraCenter[2] );

        this.update();

        //mat4.rotateY(viewMatrix, viewMatrix, zen.degToRad(-90));
    	
    } 


    this.moveForward = function(speed) {
        this.position[2] -= this.direction[2]*speed;
        //this.cameraCenter[2] += this.direction[2]*speed;

        this.position[1] -= this.direction[1]*speed;
        //this.cameraCenter[1] += this.direction[1]*speed;

        this.position[0] -= this.direction[0]*speed;
        //this.cameraCenter[0] += this.direction[0]*speed;
        this.update();
    	
    }

    this.moveBackward = function(speed) {
        this.position[2] += this.direction[2]*speed;
        //this.cameraCenter[2] += this.direction[2]*speed;

        this.position[1] += this.direction[1]*speed;
        //this.cameraCenter[1] += this.direction[1]*speed;

        this.position[0] += this.direction[0]*speed;
        //this.cameraCenter[0] += this.direction[0]*speed;
        this.update();
    	
    }

    

    this.update = function() {
        mat4.lookAt(viewMatrix, this.cameraPosition, this.cameraCenter, this.upVector);
        mat4.translate(viewMatrix, viewMatrix, this.position);
    }

    this.setMVMatrix = function() {
    	
    }

    this.calculateUpVector = function(mousePosY) {
        var documentHeight = document.outerHeight;
        var documentWidth = document.outerWidth;

        var centerX = documentWidth/2.0;
        var centerY = documentHeight/2.0;


    }
}