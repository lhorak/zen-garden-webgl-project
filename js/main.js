    var objectArray = new Array();

    var gl = null;
    var canvas = null;
    var glProgram = null;

    var mvMatrix    = mat4.create(),
        viewMatrix    = mat4.create(),
        pMatrix     = mat4.create();
    var most = null;
    var sky = null;
    var camera = null;
    var cameraMode = 0;
    var grass = null;

    var sceneObjects = {};





function initWebGL()
{


    canvas = document.getElementById("ZenGardenCanvas");  

    canvas.width  = window.innerWidth;
    canvas.height = window.innerHeight;


    try{
        gl = pgr.initialize("ZenGardenCanvas");               
    }catch(e){
        console.log(e);
    }
                    
    if(gl)
    {
        setupWebGL();
        initShaders("shader-vs", "shader-fs");



        camera = new zenCamera();
        camera.basicSetup([0,0,0]);
        //camera.turnLeft(66);

        sceneObjects.house = new zenObject();
        sceneObjects.house.createFromFile          ("../objects/dumNovy.js");
        sceneObjects.house.attachTextureFromFile   ("../textures/farmhouse.jpg");
        sceneObjects.house.translate               (3,3,-13);
        sceneObjects.house.scale                   (0.2);
        sceneObjects.house.rotateY                 (105);

        ground = new zenObject();
        sceneObjects.ground = new zenObject();
        sceneObjects.ground.createFromFile       ("../objects/groundNew.js");
        sceneObjects.ground.attachTextureFromFile("../textures/terrain.jpg");
        sceneObjects.ground.translate            (0,-5,0);
        //sceneObjects['ground'].rotateY              (45);
        sceneObjects.ground.rotateX(90);
        sceneObjects.ground.rotateY(180);
        sceneObjects.ground.scale(90);
        //sceneObjects['ground'].textureAlpha = 0.5;



        sceneObjects.bridge = new zenObject();
        sceneObjects.bridge.createFromFile("../objects/most.js");
        sceneObjects.bridge.attachTextureFromFile("../textures/most.JPG");
        sceneObjects.bridge.translate(0,10,0);
        sceneObjects.bridge.scale(1.4);
        
        getMatrixUniforms();
        initLights();
        setMatrixUniforms();

        sky = new skyBox();
        sky.init();
        sky.translate(0,0,0);
        sky.draw();
        drawSceneObjects(sceneObjects);

        update(); 
    }else{  
        alert(  "Error: Váš prohlížeč zřejmě nepodporuje WebGL.");
    }
}

function setupWebGL() {
    gl.clearColor(0.3,0.3,0.3, 1.0);
    //gl.clearColor(0, 0, 0.5, 1.0);
    gl.clearDepth(100.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE);
    gl.viewport(0, 0, canvas.width, canvas.height);

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    mat4.perspective(pMatrix,45, canvas.width / canvas.height, 0.1, 10000.0);
    mat4.identity(viewMatrix);
}

function initShaders(vsName, fsName) {
    glProgram = pgr.createProgram([
        pgr.createShaderFromElement("shader-vs"),
        pgr.createShaderFromElement("shader-fs")
    ]);

    glProgram.positionLocation      = gl.getAttribLocation(glProgram, "aVertexPosition");
    glProgram.texCoordLocation      = gl.getAttribLocation(glProgram, "aTexCoord");
    glProgram.aVertexNormal         = gl.getAttribLocation(glProgram, "aVertexNormal");

    glProgram.ourSamplerLocation    = gl.getUniformLocation(glProgram, "ourSampler");
    glProgram.uTextureAlphaValue    = gl.getUniformLocation(glProgram, "uTextureAlphaValue");


    glProgram.uLightColor           = gl.getUniformLocation(glProgram, "uLightColor");
    glProgram.uLightPosition        = gl.getUniformLocation(glProgram, "uLightPosition");
    glProgram.uAmbientColor         = gl.getUniformLocation(glProgram, "uAmbientColor");


    gl.useProgram(glProgram); 
    gl.uniform1i(glProgram.ourSamplerLocation, 0);
    gl.uniform1f(glProgram.uTextureAlphaValue, 1.0);
}

function initLights(){
    gl.uniform3fv(glProgram.uLightPosition,    [0.0, 100.0, 0.0]);
    gl.uniform3fv(glProgram.uLightColor,       [0.8,0.8,0.8]);
    gl.uniform3fv(glProgram.uAmbientColor,       [1.0,0.0,0.0]);
}


function update() {
    window.requestAnimFrame(update);
    gl.useProgram(glProgram);
    gl.clearColor(0, 0, 0.5, 1.0);
    gl.clearColor(0.3,0.3,0.3, 1.0);
    gl.clearDepth(100.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.uniform3fv(glProgram.uLightPosition,    [0.0, -1.0, -1.0]);
    gl.uniform3fv(glProgram.uLightColor,       [0.8,0.8,0.8]);
    gl.uniform3fv(glProgram.uAmbientColor,       [0.35,0.35,0.35]);
    if(cameraMode == 0) {
        if(zen.keyboard.isPressed("a") || zen.keyboard.isPressed("left_arrow")) {
            camera.turnLeft(0.8);
            //sky.position = camera.cameraCenter;
        };
        if(zen.keyboard.isPressed("d") || zen.keyboard.isPressed("right_arrow")) {
            camera.turnRight(0.8);
        };

        if(zen.keyboard.isPressed("w") || zen.keyboard.isPressed("up_arrow")) {
            camera.moveForward(0.4);
        };
        if(zen.keyboard.isPressed("s") || zen.keyboard.isPressed("down_arrow")) {
            camera.moveBackward(0.4);
        };
        camera.update();
    }
    else if(cameraMode == 1) {
        
        camera.turnRight(0.2);
        camera.update();
    }
    else if(cameraMode == 2) {
        camera.update();
    }
    sky.position = [-camera.position[0], 0, -camera.position[2]]
    sky.draw();
    drawSceneObjects(sceneObjects);
}

function drawSceneObjects(objects) {
    for (var key in objects) {
      if (objects.hasOwnProperty(key))
        objects[key].draw();
    }
}

function getMatrixUniforms(){
    glProgram.pMatrixUniform = gl.getUniformLocation(glProgram, "uPMatrix");
    glProgram.mvMatrixUniform = gl.getUniformLocation(glProgram, "uMVMatrix");      
}

function setMatrixUniforms() {
    gl.uniformMatrix4fv(glProgram.pMatrixUniform, false, pMatrix);
    gl.uniformMatrix4fv(glProgram.mvMatrixUniform, false, mvMatrix);
}

function changeCamera(cameraNumber) {
    console.log(cameraNumber);
    if(cameraNumber == null) {
        cameraMode = (cameraMode+1)%3;
    }
    else {
        cameraMode = cameraNumber;
    }

    if(cameraMode == 0) {

    }
    else if (cameraMode == 1) {
        camera.basicSetup([-2,-5,-15]);
    }
    else if (cameraMode == 2) {
        camera.basicSetup([26,-30,16]);
        camera.setAzimuth(45);
    }
}
