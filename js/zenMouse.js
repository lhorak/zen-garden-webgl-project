zen.mouse = {};

zen.mouse.posX = 0;
zen.mouse.posY = 0;

zen.mouse.getPosX = function() {
	return zen.mouse.posX;

}

zen.mouse.getPosY = function() {
	return zen.mouse.posY;
}

zen.mouse.setPosX = function(posX) {
	zen.mouse.posX = posX;
	console.log("Position X: " + zen.mouse.posX);
}

zen.mouse.setPosY = function(posY) {
	zen.mouse.posY = posY;
	console.log("Position Y: " + zen.mouse.posY);
}