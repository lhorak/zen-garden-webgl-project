zen.keyboard = {};


var keyTable = new Array();
zen.keyboard.pressedKey = null;

keyTable["a"] = false,
keyTable["b"] = false,
keyTable["c"] = false,
keyTable["d"] = false,
keyTable["e"] = false,
keyTable["f"] = false,
keyTable["g"] = false,
keyTable["h"] = false,
keyTable["i"] = false,
keyTable["j"] = false,
keyTable["k"] = false,
keyTable["l"] = false,
keyTable["m"] = false,
keyTable["n"] = false,
keyTable["o"] = false,
keyTable["p"] = false,
keyTable["q"] = false,
keyTable["r"] = false,
keyTable["s"] = false,
keyTable["t"] = false,
keyTable["u"] = false,
keyTable["v"] = false,
keyTable["w"] = false,
keyTable["x"] = false,
keyTable["y"] = false,
keyTable["z"] = false,
keyTable["0"] = false,
keyTable["1"] = false,
keyTable["2"] = false,
keyTable["3"] = false,
keyTable["4"] = false,
keyTable["5"] = false,
keyTable["6"] = false,
keyTable["7"] = false,
keyTable["8"] = false,
keyTable["9"] = false,
keyTable["left_arrow"] = false,
keyTable["right_arrow"] = false,
keyTable["up_arrow"] = false,
keyTable["down_arrow"] = false,
keyTable["backspace"] = false,
keyTable["tab"] = false,
keyTable["enter"] = false,
keyTable["shift"] = false,
keyTable["ctrl"] = false,
keyTable["alt"] = false,
keyTable["pause_break"] = false,
keyTable["caps_lock"] = false,
keyTable["escape"] = false,
keyTable["spacebar"] = false,
keyTable["page_up"] = false,
keyTable["page_down"] = false,
keyTable["end"] = false,
keyTable["home"] = false;


zen.keyboard.isPressed = function(key) {
	return keyTable[key];
}

zen.keyboard.setActive = function(key) {
	keyTable[key] = true;
}

zen.keyboard.setInactive = function(key) {
	keyTable[key] = false;
}

zen.keyboard.actionOnKeypress = function(key, callback) {
    $(document).bind('keydown', function(e) {
        //alert(e.keyCode);
        if(key == zen.keyboard.recognizeKey(e.keyCode)) {
            callback();
        }
    });
}

zen.keyboard.recognizeKey = function(keycode) {
	switch(keycode) {
    case 8:
    	return "backspace";
        break;
    case 9:
    	return "tab";
        break;
    case 13:
    	return "enter";
        break;
    case 16:
    	return "shift";
        break;
    case 17:
    	return "ctrl";
        break;
    case 18:
    	return "alt";
        break;
    case 19:
    	return "pause_break";
        break;
    case 20:
    	return "caps_lock";
        break;
    case 27:
    	return "escape";
        break;
    case 32:
    	return "spacebar";
        break;
    case 33:
    	return "page_up";
        break;
    case 34:
    	return "page_down";
        break;
    case 35:
    	return "end";
        break;
    case 36:
    	return "home";
        break;
    case 37:
    	return "left_arrow";
        break;
    case 38:
    	return "up_arrow";
        break;
    case 39:
    	return "right_arrow";
        break;
    case 40:
    	return "down_arrow";
        break;
    case 45:
    	return "insert";
        break;
    case 46:
    	return "delete";
        break;
    case 48:
    	return "0";
        break;
    case 187:
    	return "1";
        break;
    case 50:
    	return "2";
        break;
    case 51:
    	return "3";
        break;
    case 52:
    	return "4";
        break;
    case 53:
    	return "5";
        break;
    case 54:
    	return "6";
        break;
    case 55:
    	return "7";
        break;
    case 56:
    	return "8";
        break;
    case 57:
    	return "9";
        break;
    case 65:
    	return "a";
        break;
    case 66:
    	return "b";
        break;
    case 67:
    	return "c";
        break;
    case 68:
    	return "d";
        break;
    case 69:
    	return "e";
        break;
    case 70:
    	return "f";
        break;
    case 71:
    	return "g";
        break;
    case 72:
    	return "h";
        break;
    case 73:
    	return "i";
        break;
    case 74:
    	return "j";
        break;
    case 75:
    	return "k";
        break;
    case 76:
    	return "l";
        break;
    case 77:
    	return "m";
        break;
    case 78:
    	return "n";
        break;
    case 79:
    	return "o";
        break;
    case 80:
    	return "p";
        break;
    case 81:
    	return "q";
        break;
    case 82:
    	return "r";
        break;
    case 83:
    	return "s";
        break;
    case 84:
    	return "t";
        break;
    case 85:
    	return "u";
        break;
    case 86:
    	return "v";
        break;
    case 87:
    	return "w";
        break;
    case 88:
    	return "x";
        break;
    case 89:
    	return "y";
        break;
    case 90:
    	return "z";
        break;
    default:
        return -1;
        break;
} 
}

$(document).bind('keydown', function(e) {
    var keyName = zen.keyboard.recognizeKey(e.keyCode);
    zen.keyboard.setActive(keyName);
    //console.log(keyName + " active( " + keyTable[keyName] + " )");
});

$(document).bind('keyup', function(e) {
    var keyName = zen.keyboard.recognizeKey(e.keyCode);
    zen.keyboard.setInactive(keyName);
    //console.log(keyName + " inactive( " + keyTable[keyName] + " )");
});





