/**
 * Created by pupa on 04.12.14.
 */

var skyBox = function() {
    this.nVertices = null;
    this.nTriangles = null;
    this.nAttribsPerVertex = null;
    this.triangles = null;
    this.verticesInterleaved = null;

    this.texture = null;
    this.elementBuffer = null;
    this.arrayBuffer = null;
    this.normalBuffer = null;
    this.shaderProgram = null;
    this.position = mat3.create();
    this.modelMatrix = mat4.create();
    this.mvMatrix = mat4.create();

    this.Program = pgr.createProgram([
        pgr.createShaderFromElement("skybox-vs"),
        pgr.createShaderFromElement("skybox-fs")
    ]);

    this.init = function(){
        var mainObject = this;

        $.ajax({
                type: "GET",
                dataType: "json",
                async: false,
                url: '../objects/sphere.js',
                success: function (data) {
                    mainObject.nVertices = data.nVertices;
                    mainObject.nTriangles = data.nTriangles;
                    mainObject.nAttribsPerVertex = data.nAttribsPerVertex;
                    mainObject.triangles = new Uint16Array(data.triangles);

                    /*for (var i = data.verticesInterleaved.length - 1; i >= 0; i--) {
                        data.verticesInterleaved[i] = data.verticesInterleaved[i];
                    }*/

                    mainObject.verticesInterleaved = new Float32Array(data.verticesInterleaved);


                    
                    mainObject.createElementBuffer();
                    mainObject.createArrayBuffer();
                    mat4.identity(mainObject.modelMatrix);
                    mat4.identity(mainObject.mvMatrix);

                    console.log("Data z adresy: ../objects/cube.js byly úspěšně načteny.");

                },
                error: function(xhr, textStatus, error){
                    alert("Při načítání došlo k chybě. Prosím zkontrolujte URL adresu.");
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                }
            });

        mat4.identity(this.modelMatrix);
        mat4.identity(this.mvMatrix);

        this.Program.aVertexPosition       = gl.getAttribLocation(this.Program, "aVertexPosition");
        this.Program.aVertexTextureCoords  = gl.getAttribLocation(this.Program, "aVertexTextureCoords");
        this.Program.aVertexNormal         = gl.getAttribLocation(this.Program, "aVertexNormal");



        this.Program.uMVMatrix           = gl.getUniformLocation(this.Program, "uMVMatrix");
        this.Program.uPMatrix        = gl.getUniformLocation(this.Program, "uPMatrix");
        this.Program.uNMatrix         = gl.getUniformLocation(this.Program, "uNMatrix");

        //this.Program.uSampler         = gl.getUniformLocation(Program, "uSampler");
        this.Program.uCubeSampler         = gl.getUniformLocation(this.Program, "uCubeSampler");


        gl.useProgram(this.Program);

        this.createElementBuffer();
        this.createArrayBuffer();

        this.createTexture(
            '../textures/envmap/miramar_ft.tga','../textures/envmap/miramar_bk.tga',
            '../textures/envmap/miramar_dn.tga','../textures/envmap/miramar_up.tga',
            '../textures/envmap/miramar_rt.tga','../textures/envmap/miramar_lf.tga');
        //this.translate(0,60,0);
        this.rotateX(180);
        this.scale(200);

        this.draw();
    };

    this.createElementBuffer = function() {
        this.elementBuffer = gl.createBuffer();

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.elementBuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, this.triangles, gl.STATIC_DRAW);
    };

    this.createArrayBuffer = function() {
        this.arrayBuffer = gl.createBuffer();

        gl.bindBuffer(gl.ARRAY_BUFFER, this.arrayBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, this.verticesInterleaved, gl.STATIC_DRAW);
    };

    function loadCubemapFace(gl, target, texture, url) {
            var image = new Image();
            image.onload = function(){
                gl.bindTexture(gl.TEXTURE_CUBE_MAP, texture);
                gl.texImage2D(target, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
                gl.bindTexture(gl.TEXTURE_CUBE_MAP, null);
            };
            image.src = url;
        }

    this.createTexture = function(POSITIVE_X_URL, NEGATIVE_X_URL,
                                  POSITIVE_Y_URL, NEGATIVE_Y_URL,
                                  POSITIVE_Z_URL, NEGATIVE_Z_URL) {

        this.texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.texture);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

        loadCubemapFace(gl, gl.TEXTURE_CUBE_MAP_POSITIVE_X, this.texture, POSITIVE_X_URL);
        loadCubemapFace(gl, gl.TEXTURE_CUBE_MAP_NEGATIVE_X, this.texture, NEGATIVE_X_URL);
        loadCubemapFace(gl, gl.TEXTURE_CUBE_MAP_POSITIVE_Y, this.texture, POSITIVE_Y_URL);
        loadCubemapFace(gl, gl.TEXTURE_CUBE_MAP_NEGATIVE_Y, this.texture, NEGATIVE_Y_URL);
        loadCubemapFace(gl, gl.TEXTURE_CUBE_MAP_POSITIVE_Z, this.texture, POSITIVE_Z_URL);
        loadCubemapFace(gl, gl.TEXTURE_CUBE_MAP_NEGATIVE_Z, this.texture, NEGATIVE_Z_URL);
    };

    this.draw = function() {
        gl.useProgram(this.Program);
        gl.bindBuffer(gl.ARRAY_BUFFER, this.arrayBuffer);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.elementBuffer);

        this.Program.pMatrixUniform = gl.getUniformLocation(this.Program, "uPMatrix");
        this.Program.mvMatrixUniform = gl.getUniformLocation(this.Program, "uMVMatrix");
        gl.uniformMatrix4fv(this.Program.pMatrixUniform, false, pMatrix);
        gl.uniformMatrix4fv(this.Program.mvMatrixUniform, false, this.mvMatrix);

        gl.enableVertexAttribArray(this.Program.aVertexTextureCoords);
        gl.vertexAttribPointer(
            this.Program.aVertexTextureCoords, 
            2,
            gl.FLOAT,
            false,
            8 * this.verticesInterleaved.BYTES_PER_ELEMENT,
            6 * this.verticesInterleaved.BYTES_PER_ELEMENT
        );

        gl.bindTexture(gl.TEXTURE_CUBE_MAP, this.texture);
        gl.uniform1i(this.Program.uCubeSampler, 0);

        //gl.enableVertexAttribArray(this.Program.aVertexNormal);
        //gl.vertexAttribPointer(this.Program.aVertexNormal, 3, gl.FLOAT, false, 8 * this.verticesInterleaved.BYTES_PER_ELEMENT, 3 * this.verticesInterleaved.BYTES_PER_ELEMENT);

        //this.useMVMatrix();
        mat4.identity(this.modelMatrix);
        mat4.translate(this.modelMatrix,this.modelMatrix,this.position);
        this.rotateX(180);
        this.scale(90);

        gl.enableVertexAttribArray(this.Program.aVertexPosition);
        mat4.multiply(this.mvMatrix, viewMatrix, this.modelMatrix);
        gl.uniformMatrix4fv(this.Program.mvMatrixUniform, false, this.mvMatrix);

        gl.vertexAttribPointer(this.Program.aVertexPosition, 3, gl.FLOAT, gl.FALSE, 8 * this.verticesInterleaved.BYTES_PER_ELEMENT, 0);
        gl.drawElements(gl.TRIANGLES, this.nTriangles * 3, gl.UNSIGNED_SHORT, 0);
    };


    this.translate = function(x,y,z) {
        this.position = [x, y, z];
        mat4.translate(this.modelMatrix,this.modelMatrix,this.position);
    };

    this.moveForward = function(amount) {
        this.position = [x, y, z];
        mat4.translate(this.mvMatrix,this.mvMatrix,this.position);
    };

    this.rotateX = function(degrees) {
        if (degrees >= -360 && degrees <= 360) {
            this.viewAngle += degrees;
            //this.direction = vec3(Math.cos(viewAngle)), Math.sin(this.viewAngle), 0.0);
            mat4.rotateX(this.modelMatrix, this.modelMatrix, zen.degToRad(degrees));
        }
    };

    this.rotateY = function(degrees) {
        if (degrees >= -360 && degrees <= 360) {
            mat4.rotateY(this.modelMatrix, this.modelMatrix, zen.degToRad(degrees));
        }
    };

    this.rotateZ = function(degrees) {
        if (degrees >= -360 && degrees <= 360) {
            mat4.rotateZ(this.modelMatrix, this.modelMatrix, zen.degToRad(degrees));
        }
    };

    this.scale = function(x,y,z) {
        if(x && y && z) {
            this.scaleX = x;
            this.scaleY = y;
            this.scaleZ = z;
            mat4.scale(this.modelMatrix, this.modelMatrix, [this.scaleX,this.scaleY,this.scaleZ]);
        }
        else if(x && !y && !z) {
            this.scaleX = x;
            this.scaleY = x;
            this.scaleZ = x;
            mat4.scale(this.modelMatrix, this.modelMatrix, [this.scaleX,this.scaleY,this.scaleZ]);
        }
        else {
            return 1;
        }
    }

};


